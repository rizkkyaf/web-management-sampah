<?php

include('database.php');
include('crud.php');

class API extends Database implements Crud
{

 public function fetch_all()
 {
  $query = "SELECT * FROM types ORDER BY id";
  $statement = $this->connect->prepare($query);
  if($statement->execute())
  {
   while($row = $statement->fetch(PDO::FETCH_ASSOC))
   {
    $data[] = $row;
   }
   return $data;
  }
 }

 public function insert()
 {
  if(isset($_POST["name"]))
  {
   $form_data = array(
    ':name'  => $_POST["name"],
    ':type'  => $_POST["type"]
   );
   $query = "INSERT INTO types (name, type) VALUES (:name, :type)";
   $statement = $this->connect->prepare($query);
   if($statement->execute($form_data))
   {
    $data[] = array(
     'success' => '1'
    );
   }
   else
   {
    $data[] = array(
     'success' => '0'
    );
   }
  }
  else
  {
   $data[] = array(
    'success' => '0'
   );
  }
  return $data;
 }

 function delete($id)
 {
  $query = "DELETE FROM types WHERE id = '".$id."'";
  $statement = $this->connect->prepare($query);
  if($statement->execute())
  {
   $data[] = array(
    'success' => '1'
   );
  }
  else
  {
   $data[] = array(
    'success' => '0'
   );
  }
  return $data;
 }
}

?>