$(document).ready(function()
{
 fetch_data();
 fetch_data_material();

 function fetch_data()
 {
  $.ajax({
   url:"fetch.php",
   success:function(data)
   {
    $('#fetch-data').html(data);
   }
  })
 }

 function fetch_data_material()
 {
    $.ajax({
   url:"fetch_material.php",
   success:function(data)
   {
    $('select').html(data);
   }
  })
 }

 $('#add_button').click(function()
 {
  $('#action').val('insert');
  $('#button_action').val('Insert');
  $('#apicrudModal').modal('show');
 });

 $('#api_crud_form').on('submit', function(event)
 {
  event.preventDefault();
  if($('#name').val() == '')
  {
   alert("Masukkan Kategori");
  }
  else if($('#type').val() == '')
  {
   alert("Field Nama Tidak Boleh Kosong");
  }
  else
  {
   var form_data = $(this).serialize();
   //console.log(form_data);
   $.ajax({
    url:"action.php",
    method:"POST",
    data:form_data,
    success:function(data)
    {
     fetch_data();
     $('#api_crud_form')[0].reset();
     $('#apicrudModal').modal('hide');
     console.log(data);
     if(data == 'insert')
     {
         alert("Data Berhasil Disimpan");
     }
     else{
         alert("Data Gagal Disimpan");
     }
    }
    });
    }    
 });

 $(document).on('click', '.delete', function()
 {
  var id = $(this).attr("id");
  var action = 'delete';
  if(confirm("Hapus Data Ini ?"))
  {
   $.ajax({
    url:"action.php",
    method:"POST",
    data:{id:id, action:action},
    success:function(data)
    {
     fetch_data();
     alert("Data Telah Dihapus");
    }
   });
  }
 });

});