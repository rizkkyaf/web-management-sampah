<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>WEB MANAGEMENT SAMPAH</title>
  </head>
  <body>
      <div class="container mt-2" style="background-color: #E5E5E5;">
          <div class="row">
              <div class="col">
                <h1 class="text-center my-5 font-monospace fw-bold" style="color: #1C85A2;">WEB MANAGEMENT SAMPAH</h1>
                <div class="row justify-content-center">
                    <div class="col-4">
                        <div class="row" style="color: aliceblue; background-color: #0C325F;">
                            <div class="col">
                                <h3 class="text-center py-3 col-12 fw-light fs-5" >Data Sampah</h3>
                            </div>
                        </div>
                        <div class="row justify-content-center bg-light mb-3">
                            <div class="col-12">                                
                                <div class="row justify-content-center">
                                    <div class="col-12">
                                        <button type="button" class="btn btn-info p-3 shadow rounded-3 col-12 my-3 " style="color:aliceblue; background-color: #30AEE4;" data-bs-toggle="modal" data-bs-target="#addModal" >Tambah</button>
                                    </div>
                                </div>
                                <div id="fetch-data">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
          </div>
      </div>

      <!-- Modal -->
        <div class="modal fade" id="addModal" tabindex="-1" aria-labelledby="addModalLabel" aria-hidden="true">
            <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                <!-- <h5 class="modal-title text-center" id="addModalLabel">Input Sampah</h5> -->
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <div class="row justify-content-center">
                        <div class="col-10">
                            <div class="row" style="color: aliceblue; background-color: #0C325F;">
                                <div class="col">
                                    <h3 class="text-center py-3 col-12 fw-light fs-5" >Input Sampah</h3>
                                </div>
                            </div>
                            <div class="row justify-content-center bg-light mb-3">
                                <div class="col-12">                                
                                    <form method="POST" id="api_crud_form">
                                        <label for="category" class="form-label">Kategori Sampah</label>
                                        <select class="form-select" id="name" name="name" aria-label="Default select example" placeholder="Kategori">

                                        </select>
                                        <div class="my-3">
                                        <label for="category" class="form-label">Nama Sampah</label>
                                        <input type="text" class="form-control" id="type" name="type" aria-describedby="categoryHelp" placeholder="Nama Sampah">
                                        </div>
                                        <input type="hidden" name="action" id="action" value="insert" />  
                                        <input type="submit" name="button_action" id="button_action" class="btn my-3 p-3 col-12 rounded-3" style="color:aliceblue; background-color: #30AEE4;" value="Insert"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="js/index.js"></script>

  </body>
</html>