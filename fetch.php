<?php

//fetch.php

$api_url = "http://localhost/web-management-sampah/test_api.php?action=fetch_all";

$client = curl_init($api_url);

curl_setopt($client, CURLOPT_RETURNTRANSFER, true);

$response = curl_exec($client);
//var_dump($response);

$result = json_decode($response);
//var_dump($result);

$output = '';

if(count($result) > 0)
{
 foreach($result as $row)
 {
  $output .= '
  <div class="row justify-content-center my-2">
                                    <div class="col-11 p-2 my-2 bg-light shadow rounded-3">
                                        <p class="text-start mx-2 fs-5">'.$row->type.'</p>
                                        <div class="row">
                                            <div class="col-6">
                                                <p class="text-start text-muted mx-2 fs-6">'.$row->name.'</p>
                                            </div>
                                            <div class="col-6 justify-content-center">
                                                <p class="text-end">
                                                    <button type="button" name="delete" class="btn btn-danger btn-xs delete" id="'.$row->id.'">Hapus</button>
                                                </p>
                                            </div>
                                        </div>
        
                                    </div>
                                </div>
  ';
 }
}
else
{
 $output .= '
 <tr>
  <td colspan="4" align="center">No Data Found</td>
 </tr>
 ';
}

echo $output;

?>
