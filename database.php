<?php

abstract class Database
{
 protected $connect;

 function __construct()
 {
  $this->database_connection();
 }

 public function database_connection()
 {
  $this->connect = new PDO("mysql:host=localhost;dbname=dbrest", "root", "");
 }

 abstract public function fetch_all();
 
}

?>