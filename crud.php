<?php 

interface Crud 
{
    public function insert();
    public function delete($id);
}

?>