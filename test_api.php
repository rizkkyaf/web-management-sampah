
<?php

include('api.php');

$api_object = new API();
$api_object->database_connection();
$api_object->fetch_all();

if($_GET["action"] == 'fetch_all')
{
 $data = $api_object->fetch_all();
}

if($_GET["action"] == 'insert')
{
 $data = $api_object->insert();
}

if($_GET["action"] == 'delete')
{
 $data = $api_object->delete($_GET["id"]);
}

echo json_encode($data);

?>